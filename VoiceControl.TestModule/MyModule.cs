﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VoiceControl.ExternalInterface;

namespace VoiceControl.TestModule
{
    public class MyModule : IModule
    {
        private IApplication _app;

        public string Name()
        {
            return "Test Module";
        }

        public IEnumerable<string> Commands()
        {
            return new List<string>() { "SayHello" };
        }

        public Window GetMainWindow()
        {
            return null;
        }

        public void Run(string command)
        {
            MessageBox.Show("Hello from Module");
        }

        public void SetApplication(IApplication app)
        {
            _app = app;
        }
    }
}
