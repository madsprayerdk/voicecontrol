﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Speech.Recognition;
using System.Windows;
using VoiceControl.BaseCommands;
using VoiceControl.ExternalInterface;

namespace VoiceControl
{
    public partial class App : Application, IApplication
    {
        private Window _currentWindow;

        private readonly SpeechRecognizer _recognizer;

        private Grammar _baseCommands;
        private readonly Dictionary<string, IBaseCommand> _baseCommandList;

        private readonly Dictionary<string, IModule> _modules;
        private Grammar _loadModuleGrammar;

        private Grammar _moduleGrammar;
        private IModule _loadedModule;
        private readonly IList<string> _moduleCommands; 

        public App()
        {
            _currentWindow = new MainWindow();
            _currentWindow.Show();

            LogThis("Initializing Application");
            _recognizer = new SpeechRecognizer();
            _recognizer.SpeechRecognized += recognizer_SpeechRecognized;
            _baseCommandList = new Dictionary<string, IBaseCommand>();
            _modules = new Dictionary<string, IModule>();
            _moduleCommands = new List<string>();

            LoadModules();
            LoadBaseCommands();
        }

        private void recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (_baseCommandList.ContainsKey(e.Result.Text))
            {
                var command = _baseCommandList[e.Result.Text];
                command.Run(e.Result.Text);
            }
            else if (e.Result.Text.StartsWith("Load module:"))
            {
                LoadModule(e.Result.Text.Substring(12));
            }
            else if (e.Result.Text == "Reload modules")
            {
                LoadModules();
            }
            else if (_moduleCommands.Contains(e.Result.Text))
            {
                _loadedModule.Run(e.Result.Text);
            }
        }

        private void LoadBaseCommands()
        {
            LogThis("Loading Base Commands");
            if (_baseCommands != null)
                _recognizer.UnloadGrammar(_baseCommands);

            _baseCommandList.Clear();

            var commandList = new Choices();

            var commands = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.Namespace != null && x.Namespace.StartsWith("VoiceControl.BaseCommands"));

            foreach (var command in commands)
            {
                if (command.IsClass)
                {
                    var com = Activator.CreateInstance(command) as IBaseCommand;

                    if (com == null)
                        continue;

                    com.SetApplication(this);

                    foreach (var command1 in com.Commands())
                    {
                        LogThis("Loaded command: " + command1);
                        commandList.Add(command1);
                        _baseCommandList.Add(command1, com);
                    }
                }
            }

            _baseCommands = new Grammar(commandList) { Priority = 127 };
            _recognizer.LoadGrammar(_baseCommands);
        }

        public void LoadModules()
        {
            LogThis("Loading Modules");
            if (!Directory.Exists("Modules/"))
            {
                LogThis("Can't find modules directory");
                return;
            }

            if(_loadModuleGrammar != null)
                _recognizer.UnloadGrammar(_loadModuleGrammar);
                
            var modules = new Choices();
            LogThis("Reading modules directory");
            var files = Directory.GetFiles("Modules/", "*.dll").ToList();
            LogThis("Number of files in module folder: " + files.Count);

            var envVariables = Environment.GetCommandLineArgs();

            if (envVariables.Length > 1)
            {
                LogThis("Loading Envrioment Variables");
                for (int i = 1; i < envVariables.Length; i++)
                {
                    LogThis("Loading Envrioment Variable: " + envVariables[i]);
                    if (File.Exists(envVariables[i]))
                        files.Add(envVariables[i]);
                }
            }

            foreach (var file in files)
            {
                LogThis("Found module file: " + file);
                Assembly assembly;

                try
                {
                    assembly = Assembly.LoadFrom(file);
                }
                catch (BadImageFormatException)
                {
                    LogThis("Unable to load module: " + file);
                    continue;
                }

                var moduleInterface = typeof(IModule);
                var moduleType = assembly.GetTypes().FirstOrDefault(x => x.IsClass && moduleInterface.IsAssignableFrom(x));
                if (moduleType == null)
                    continue;

                var module = Activator.CreateInstance(moduleType) as IModule;
                if(module == null)
                    continue;

                LogThis("Loaded Module: " + module.Name());

                _modules.Add(module.Name(), module);
                modules.Add("Load module:" + module.Name());
            }

            _loadModuleGrammar = new Grammar(modules) { Priority = 127 };
            _recognizer.LoadGrammar(_loadModuleGrammar);
        }

        public void LoadModule(string moduleName)
        {
            LogThis("Loading new active module");
            var module = _modules[moduleName];

            if(_moduleGrammar != null)
                _recognizer.UnloadGrammar(_moduleGrammar);

            _currentWindow.Dispatcher.Invoke(() =>
            {
                //if (_currentWindow.IsActive)
                    //_currentWindow.Close();
            });
            
            _moduleCommands.Clear();
            var moduleChoises = new Choices();
            foreach (var command in module.Commands())
            {
                moduleChoises.Add(command);
                _moduleCommands.Add(command);
            }

            LogThis("New active module: " + module.Name());

            _loadedModule = module;
            _moduleGrammar = new Grammar(moduleChoises);
            _recognizer.LoadGrammar(_moduleGrammar);
        }

        public Window CurrentWindow()
        {
            return _currentWindow;
        }

        private void LogThis(string message)
        {
            var window = (MainWindow) _currentWindow;

            window.Dispatcher.Invoke(() =>
            {
                window.LogMessage(message);
            });
        }
    }
}
