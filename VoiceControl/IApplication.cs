﻿using System.Windows;

namespace VoiceControl
{
    public interface IApplication
    {
        Window CurrentWindow();
        void LoadModules();
        void LoadModule(string moduleName);
    }
}
