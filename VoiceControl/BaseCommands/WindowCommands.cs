﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VoiceControl;

namespace VoiceControl.BaseCommands
{
    public class WindowCommands : IBaseCommand
    {
        private IApplication _app;

        private readonly Dictionary<string, WindowState> _parameters;

        public WindowCommands()
        {
            _parameters = new Dictionary<string, WindowState>
            {
                    { "Maximize", WindowState.Maximized },
                    { "Normal Window", WindowState.Normal },
                    { "Minimize", WindowState.Minimized }
            };
        }

        public IEnumerable<string> Commands()
        {
            return _parameters.Select(x => x.Key).ToList();
        }

        public void Run(string command)
        {
            _app.CurrentWindow().Dispatcher.Invoke(() =>
            {
                _app.CurrentWindow().WindowState = _parameters[command];
            });
        }

        public void SetApplication(IApplication app)
        {
            _app = app;
        }
    }
}
