﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using VoiceControl;

namespace VoiceControl.BaseCommands
{
    public class MoveToScreenCommand : IBaseCommand
    {
        private IApplication _app;

        private Dictionary<string, bool> _parameters;

        public MoveToScreenCommand()
        {
            _parameters = new Dictionary<string, bool>
            {
                { "Move to primary", true },
                { "Move to primary screen", true },
                { "Move to primary monitor", true },
                { "Move to secondary", false },
                { "Move to secondary screen", false },
                { "Move to secondary monitor", false }
            };
        }

        public IEnumerable<string> Commands()
        {
            return _parameters.Select(x => x.Key).ToList();
        }

        public void Run(string command)
        {
            var monitor = _parameters[command]
                ? Screen.AllScreens.FirstOrDefault(x => x.Primary)
                : Screen.AllScreens.FirstOrDefault(x => !x.Primary);

            if (monitor != null)
            {
                var workingArea = monitor.WorkingArea;

                _app.CurrentWindow().Dispatcher.Invoke(() =>
                {
                    _app.CurrentWindow().WindowState = WindowState.Normal;
                    _app.CurrentWindow().Left = workingArea.Left;
                    _app.CurrentWindow().Top = workingArea.Top;
                    _app.CurrentWindow().Width = workingArea.Width;
                    _app.CurrentWindow().Height = workingArea.Height;
                    _app.CurrentWindow().WindowState = WindowState.Maximized;
                });
            }
        }

        public void SetApplication(IApplication app)
        {
            _app = app;
        }
    }
}
