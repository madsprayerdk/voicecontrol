﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoiceControl;

namespace VoiceControl.BaseCommands
{
    public interface IBaseCommand
    {
        IEnumerable<string> Commands();
        void Run(string command);
        void SetApplication(IApplication app);
    }
}
