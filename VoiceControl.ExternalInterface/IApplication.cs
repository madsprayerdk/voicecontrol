﻿using System.Windows;

namespace VoiceControl.ExternalInterface
{
    public interface IApplication
    {
        Window CurrentWindow();
    }
}
