﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VoiceControl.ExternalInterface
{
    public interface IModule
    {
        string Name();
        IEnumerable<string> Commands();
        Window GetMainWindow();
        void Run(string command);
        void SetApplication(IApplication app);
    }
}
